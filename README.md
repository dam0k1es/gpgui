# Graphical Interface for GPG

![gpgui.png](https://gitlab.com/dam0k1es/gpgui/-/raw/main/gpgui.png)

Developed under: Pop!_OS, Arch\
Installation: see build.sh

gpgui is a script I made to teach Students about the GNU Privacy Guard. \
It is written in python and depends on "python-gnupg", "PySide6" and "gpg".\
The script offers the possibility to interact with gpg and to learn\
basics about QT-GUI's and getopt.

# CAN
- encypt, or decrypt a string, or file
- import, or export a public key
- send cipher text to clipboard
- auto-use an imported key
- change the recipient
- un/hide the passphrase
- be used as cli / gui only

# RESOURCES
- gpg: 	      
  - gnupg.org
  - digitalocean.com/community/tutorials/how-to-use-gpg-to-encrypt-and-sign-messages
- gnupg:		  
  - pythonhosted.org/gnupg/gnupg.html
- Python Qt:  
  - dc.qt.io/qt-5
